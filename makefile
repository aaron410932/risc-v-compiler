all:
	byacc -d -v parser.y
	flex scanner.l
	gcc -o codegen lex.yy.c y.tab.c code.c -lfl

clean:
	rm -f codegen lex.yy.c y.output y.tab.c y.tab.h