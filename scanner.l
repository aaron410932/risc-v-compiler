%{
#include <stdio.h>
#include <string.h>

int lineCount=0;
char line[300] = "";
int source = 1;
int token = 1;

#include "y.tab.h"
%}

KEYWORDS "for"|"do"|"while"|"break"|"continue"|"if"|"else"|"return"|"struct"|"switch"|"case"|"default"|"void"|"int"|"double"|"float"|"char"|"const"|"signed"|"unsigned"|"short"|"long"
MACROS   "NULL"|"__COUNTER__"|"__LINE__"|"INT_MAX"|"INT_MIN"|"CHAR_MAX"|"CHAR_MIN"|"MAX"|"MIN"|"HIGH"|"LOW"
OPERATORS   "+"|"-"|"*"|"/"|"%"|"++"|"--"|"<"|"<="|">"|">="|"=="|"!="|"="|"&&"|"||"|"!"|"&"|"|"|"^"|"~"|"["|"]"|"("|")"|">>"|"<<"
ID       [a-zA-Z_][a-zA-Z0-9_]*
STRING   \"([^\\\"]|\\.)*\"
PUNCT   ":"|";"|","|"."|"["|"]"|"("|")"|"{"|"}"
FLOAT   (([0-9]*\.[0-9]+)|[0-9]+\.[0-9]*)
INTEGER [0-9]+
CHAR    '([^\\]|\\([abefnrtv\\'\"\?]|[0-7]{1,3}|x[0-9A-Fa-f]+|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8}))'

%x SINGLECOMMENT
%x DOUBLECOMMENT

%%

\n  {
    lineCount++;
    line[0] = '\0';
    
}

\r  {

}

{KEYWORDS} {
    if(strcmp(yytext, "for") == 0) return FOR;
    else if(strcmp(yytext, "do") == 0) return DO;
    else if(strcmp(yytext, "while") == 0) return WHILE;
    else if(strcmp(yytext, "break") == 0) return BREAK;
    else if(strcmp(yytext, "continue") == 0) return CONTINUE;
    else if(strcmp(yytext, "if") == 0) return IF;
    else if(strcmp(yytext, "else") == 0) return ELSE;
    else if(strcmp(yytext, "return") == 0) return RETURN;
    else if(strcmp(yytext, "switch") == 0) return SWITCH;
    else if(strcmp(yytext, "case") == 0) return CASE;
    else if(strcmp(yytext, "default") == 0) return DEFAULT;
    else if(strcmp(yytext, "void") == 0) return VOID;
    else if(strcmp(yytext, "int") == 0) return INT;
    else if(strcmp(yytext, "double") == 0) return DB;
    else if(strcmp(yytext, "float") == 0) return FT; 
    else if(strcmp(yytext, "char") == 0) return CH; 
    else if(strcmp(yytext, "const") == 0) return CONST;
    else if(strcmp(yytext, "signed") == 0) return SIGNED;
    else if(strcmp(yytext, "unsigned") == 0) return UNSIGNED;
    else if(strcmp(yytext, "short") == 0) return SHORT;
    else if(strcmp(yytext, "long") == 0) return LONG;
}

{MACROS} {
    if(strcmp(yytext, "NULL") == 0) return NL;
    else if(strcmp(yytext, "HIGH") == 0) return HIGH;
    else if(strcmp(yytext, "LOW") == 0) return LOW;
    
}

{OPERATORS} {
    
    if(strcmp(yytext, ">=") == 0) return GE;
    else if(strcmp(yytext, "<=") == 0) return LE;
    else if(strcmp(yytext, "==") == 0) return EQ;
    else if(strcmp(yytext, "!=") == 0) return NE;
    else if(strcmp(yytext, "&&") == 0) return DAND;
    else if(strcmp(yytext, "||") == 0) return DOR;
    else if(strcmp(yytext, "++") == 0) return INC;
    else if(strcmp(yytext, "--") == 0) return DEC;
    else if(strcmp(yytext, ">>") == 0) return RRIGHT;
    else if(strcmp(yytext, "<<") == 0) return LLEFT;
    else if(strlen(yytext) == 1) return *yytext;


}

{ID} {
    yylval.sym = strdup(yytext); return ID;
}

{PUNCT} {
    return *yytext;
}

{STRING} {
    yylval.sym = strdup(yytext); return STRING;
}

{INTEGER} {
    yylval.sym = strdup(yytext); return INTEGER;
}

{CHAR} {
    yylval.sym = strdup(yytext); return CHAR;
}

{FLOAT} {
    yylval.sym = strdup(yytext); return FLOAT;
}

"//" {
    BEGIN SINGLECOMMENT;
}

<SINGLECOMMENT>\n {
    lineCount++;
    line[0] = '\0';
    BEGIN 0;
}

<SINGLECOMMENT>\r {
}

<SINGLECOMMENT>. {

}

"/*" {
    BEGIN DOUBLECOMMENT;

}

<DOUBLECOMMENT>"*/" {

    BEGIN 0;
}

<DOUBLECOMMENT>\n {
    lineCount++;
    line[0] = '\0';
}

<DOUBLECOMMENT>\r {

}

<DOUBLECOMMENT>. {

}

" "|\t {
}

%%
/* 
int main(int argc, char* argv[])
{
    yylex();
    return 0;
} */