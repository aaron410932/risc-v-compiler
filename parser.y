%{
    #include <stdio.h>
    #include <string.h>
    #include "./code.h"

    int invoke_num = 0;
    int ifnum = 0;
    int whilenum = 0;
    int donum = 0;
    int fornum = 0;

    struct break_entry {
        int type;
        int num;
    }  break_table[MAX_TABLE_SIZE];

    int breakindex = 0;
    char *functor_name;
%}

%union{
    int intVal;
    double dval;
    char* sym;
}

%type<sym> id
%type<intVal> expr array_expr if_branch init_while init_do init_for invoke_paras

%token FOR DO WHILE BREAK CONTINUE IF RETURN SWITCH CASE DEFAULT VOID CONST NL INT DB FT CH SIGNED UNSIGNED SHORT LONG INC DEC HIGH LOW
%token<sym> INTEGER CHAR STRING ID FLOAT
%nonassoc IFX
%nonassoc ELSE
%right '='
%left DOR
%left DAND
%left '|'
%left '^'
%left '&'
%left EQ NE
%left '>' GE '<' LE
%left RRIGHT LLEFT
%left '+' '-'
%left '*' '/' '%'
%right '!' '~' INC DEC UPLUS UMINUS CAST DEREF ADDROF
%left POSTINC POSTDEC '(' ARRSUB

%%

global_decl:
    global_decl scalar_decl {}
    |global_decl array_decl {}
    |global_decl func_decl  {}
    |global_decl func_def   {}
    |
;

/* scalar declaration */

scalar_decl:
    type_expr indents ';'{}
;

type_expr:
    const_expr sign_expr LONG LONG INT {}
    |const_expr sign_expr LONG INT {}
    |const_expr sign_expr SHORT INT {}
    |const_expr sign_expr INT {}
    |const_expr sign_expr LONG LONG {}
    |const_expr sign_expr LONG {}
    |const_expr sign_expr SHORT {}
    |const_expr sign_expr CH {}
    |const_expr SIGNED {}
    |const_expr UNSIGNED {}
    |const_expr FT {}
    |const_expr DB {}
    |const_expr VOID {}
    |CONST {}
    
;

const_expr:
    CONST {}
    | {}
;

sign_expr:
    SIGNED {}
    |UNSIGNED {}
    | {}
;

indents:
    indents ',' indent{}
    |indent {}
    
;

indent:
    id '=' expr{
        add_local_var();
        if($3 < 0){
            printf("\tld t0, %d(s0)\n", $3);
            printf("\taddi sp, sp, -8\n");
            printf("\tsd t0, 0(sp)\n");
        } else if($3 > 0){
            printf("\tld t0, 0(sp)\n");
            if($3 < 1000){
                for(int i = 0; i < $3; i++){
                    printf("\tld t0, 0(t0)\n");
                }
            }
            printf("\tsd t0, 0(sp)\n");
        } 
    }
    |id {
        add_local_var();
        printf("\taddi sp, sp, -8\n");}
;

id:
    ID { $$ = $1; install_symbol($1); }
    |'*' ID { $$ = $2; install_symbol($2); }
;

/* array declaration */

array_decl: 
    type_expr arrays ';'{}
;

arrays:
    arrays ',' arr '=' '{' arr_content '}'{}
    |arrays ',' arr {}
    |arr '=' '{' arr_content '}' {}
    |arr {}

;

arr:
    id '[' INTEGER ']' {
        add_local_arr(atoi($3));
        printf("\taddi sp, sp, -%d\n", atoi($3)*8);}
    | arr '[' expr ']' {}
;

arr_content:
    arr_content ',' expr{}
    |arr_content ',' '{' arr_content '}'{}
    |expr{}
    |'{' arr_content '}' {}
;

/* function declaration */

func_decl:
    type_expr id '(' func_paras ')' ';' {}
    |type_expr id '(' ')' ';' {}

func_paras:
    func_paras ',' type_expr id{}
    |type_expr id{
    }

/* function_definition */
func_def:
    type_expr id '(' func_paras ')' {
        cur_scope++; 
        set_scope_and_offset_of_param($2);
        code_gen_func_header($2);
        functor_name = copys($2);
        }compound_stmt{
            pop_up_symbol(cur_scope);
            cur_scope--;
            code_gen_at_end_of_function_body($2);
        }
    |type_expr id '(' ')' {
        cur_scope++; 
        set_scope_and_offset_of_param($2);
        code_gen_func_header($2);
    }compound_stmt{
        pop_up_symbol(cur_scope);
        cur_scope--;
        code_gen_at_end_of_function_body($2);
    }
;

/* expression */

expr:
    expr '+' expr {
        get_two_expr($1, $3);
        
        if($1 >= 1000){
            printf("\tli t2, 8\n");
            printf("\tmul t0, t0, t2\n");
        }
        printf("\tadd t0, t0, t1\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = ($1 >= 1000) ? 1000 : 0;
    }
    |expr '-' expr {
        get_two_expr($1, $3);
        if($1 >= 1000){
            printf("\tli t2, 8\n");
            printf("\tmul t0, t0, t2\n");
        }
        
        printf("\tsub t0, t1, t0\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = ($1 >= 1000) ? 1000 : 0;
    }
    |expr '*' expr {
        get_two_expr($1, $3);
        
        printf("\tmul t0, t0, t1\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr '/' expr {
        get_two_expr($1, $3);
        
        printf("\tdiv t0, t1, t0\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr '%' expr {}
    |INC expr {}
    |DEC expr {}
    |expr INC %prec POSTINC{}
    |expr DEC %prec POSTDEC{}
    |expr '<' expr {
        get_two_expr($1, $3);

        printf("\tslt t0, t1, t0\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr '>' expr {
        get_two_expr($1, $3);

        printf("\tslt t0, t0, t1\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr LE expr {
        get_two_expr($1, $3);

        printf("\tslt t0, t0, t1\n");
        printf("\tsnez t0, t0\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr GE expr {
        get_two_expr($1, $3);

        printf("\tslt t0, t1, t0\n");
        printf("\tsnez t0, t0\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr EQ expr {
        get_two_expr($1, $3);

        printf("\txor t0, t0, t1\n");
        printf("\tseqz t0, t0\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr NE expr {
        get_two_expr($1, $3);

        printf("\txor t0, t0, t1\n");
        printf("\tsnez t0, t0\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr '=' expr {
        if($3 >= 0){
            printf("\tld t0, 0(sp)\n");
            for(int i = 0; i < $3; i++){
                printf("\tld t0, 0(t0)\n");
            }
            printf("\taddi sp, sp, 8\n");
        } else {
            printf("\tld t0, %d(s0)\n", $3);\
        }

        if($1 > 0){
            printf("\tld t1, 0(sp)\n");
            for(int i = 1; i < $1; i++)
                printf("\tld t1, 0(t1)\n");
            printf("\taddi sp, sp, 8\n");
            printf("\tsd t0, 0(t1)\n");
        } else if($1 < 0){
            printf("\tsd t0, %d(s0)\n", $1);
        }

        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr DAND expr {}
    |expr DOR expr {}
    |'!' expr {}
    |'~' expr {}
    |'+' expr %prec UPLUS {}
    |'-' expr %prec UMINUS {
        if($2 >= 0){
            printf("\tld t0, 0(sp)\n");
            for(int i = 0; i < $2; i++){
                printf("\tld t0, 0(t0)\n");
            }
        } else {
            printf("\tld t0, %d(s0)\n", $2);
            printf("\taddi sp, sp, -8\n");
        }
        printf("\tsub t0, zero, t0\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0;
    }
    |expr '^' expr {}
    |expr '&' expr {}
    |expr '|' expr {}
    |expr RRIGHT expr {}
    |expr LLEFT expr {}
    |array_expr{ $$ = $1; }
    |'(' expr ')' {printf("\t//(expr) finished\n"); $$ = $2;}
    |'*' expr  %prec DEREF{
        if($2 < 0){ //Not a value store in the stack top
            printf("\tld t0, %d(s0)\n", $2);
            printf("\taddi sp, sp, -8\n");
            printf("\tsd t0, 0(sp)\n");
            $$ = 1;
        } else {
            $$ = ($2 < 1000) ? $2+1 : 1; //seen as address
        }
    }
    |'&' expr  %prec ADDROF{ //suppose expr is ID here
        printf("\taddi t0, s0, %d\n", $2);
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 0; //seen as value
    }
    |'(' type_expr ')' expr %prec CAST{}
    |'(' type_expr '*' ')' expr %prec CAST{}
    |ID '(' invoke_paras ')'{
        for(int i = $3-1; i >= 0; i--){
            printf("\tld a%d, 0(sp)\n", i);
            printf("\taddi sp, sp, 8\n");
        }
        printf("\taddi sp, sp, -8\n");
        printf("\tsd ra, 0(sp)\n");
        printf("\tjal ra, %s\n", $1);
        printf("\tld ra, 0(sp)\n");
        printf("\tsd a0, 0(sp)\n");
        $$ = 0;
    }
    |ID '(' ')'{
        printf("\taddi sp, sp, -8\n");
        printf("\tsd ra, 0(sp)\n");
        printf("\tjal ra, %s\n", $1);
        printf("\tld ra, 0(sp)\n");
        printf("\tsd a0, 0(sp)\n");
        $$ = 0;
    }
    |NL {}
    |HIGH {printf("\tli t0, 1\n\taddi sp, sp, -8\n\tsd t0, 0(sp)\n"); $$ = 0;}
    |LOW {printf("\tli t0, 0\n\taddi sp, sp, -8\n\tsd t0, 0(sp)\n"); $$ = 0;}
    |ID {
        int index, functor_index;
                
        index=look_up_symbol($1);
        
        switch(table[index].mode) {
            case ARGUMENT_MODE:
                $$ = -104-table[index].offset*8;
                break;
            case LOCAL_MODE:
                functor_index = table[index].functor_index;
                if(table[index].type == VARIABLE) {
                    $$ = -104-table[functor_index].total_args*8-table[index].offset*8;
                }
                else if(table[index].type == ARRAY){
                    printf("\taddi t0, s0, %d\n", -104-table[functor_index].total_args*8-table[index].offset*8);
                    printf("\taddi sp, sp, -8\n");
                    printf("\tsd t0, 0(sp)\n");
                    $$ = 1000;
                }
                break;
        }
    }
    |INTEGER {printf("\tli t0, %s\n\taddi sp, sp, -8\n\tsd t0, 0(sp)\n", $1); $$ = 0;}
    |CHAR {}
    |FLOAT {}
    |STRING {}
;

array_expr:
    ID '[' expr ']'{
        int index, functor_index;
                
        index=look_up_symbol($1);
        
        switch(table[index].mode) {
            case ARGUMENT_MODE:
                printf("\tld t0, %d(s0)\n", -104-table[index].offset*8);
                break;
            case LOCAL_MODE:
                functor_index = table[index].functor_index;
                printf("\taddi t0, s0, %d\n", -104-table[functor_index].total_args*8-table[index].offset*8);
                break;
        }
        get_one_expr_to_temp($3, 1);
        printf("\tli t2, 8\n");
        printf("\tmul t1, t1, t2\n");
        printf("\tadd t0, t0, t1\n");
        printf("\taddi sp, sp, -8\n");
        printf("\tsd t0, 0(sp)\n");
        $$ = 1;
        break;
    }
    |array_expr '[' expr ']'{}

invoke_paras:
    expr {
        if($1 > 0){
            printf("\tld t0, 0(sp)\n");
            if($1 < 1000){
                for(int i = 0; i < $1; i++){
                    printf("\tld t0, 0(t0)\n");
                }
            }
            printf("\tsd t0, 0(sp)\n");
        } else if($1 < 0){
            printf("\tld t0, %d(s0);\n", $1);
            printf("\taddi sp, sp, -8\n");
            printf("\tsd t0, 0(sp)\n");
        } $$ = 1;}
    |invoke_paras ',' expr {
        if($3 > 0){
            printf("\tld t0, 0(sp)\n");
            if($3 < 1000){
                for(int i = 0; i < $3; i++){
                    printf("\tld t0, 0(t0)\n");
                }
            }
            printf("\tsd t0, 0(sp)\n");
        } else if($3 < 0) {
            printf("\tld t0, %d(s0);\n", $3);
            printf("\taddi sp, sp, -8\n");
            printf("\tsd t0, 0(sp)\n");
        } $$ = $1 + 1;}

stmt:
    expr ';' {if($1 >= 0) printf("\taddi sp, sp, 8\n"); else printf("\n");}
    |ifelse_stmt {printf("\n");}
    |SWITCH '(' expr ')' '{' switch_clauses '}' {}
    |SWITCH '(' expr ')' '{' '}' {}
    |WHILE init_while con_expr ')' {
        printf("\tbeqz t0, WHILEEND%d\n", $2);
    } stmt {
        pop_up_symbol(cur_scope--);
        breakindex--;
        printf("\tjal zero, WHILESTART%d\n", $2);
        printf("WHILEEND%d:\n", $2);
    }
    |init_do stmt WHILE '(' con_expr ')' ';' {
        pop_up_symbol(cur_scope--);
        breakindex--;
        printf("\tbnez t0, DOSTART%d\n", $1);
        printf("DOEND%d:\n", $1);
    }
    |FOR init_for for_expr ';' {
        printf("FORCON%d:\n", $2);
    }
    con_expr {
        printf("\tbeqz t0, FOREND%d\n", $2);
        printf("\tjal zero, FORSTART%d\n", $2);
    }';' {
        printf("FORLOOP%d:\n", $2);
    } for_expr {
        printf("\tjal zero, FORCON%d\n", $2);
    } ')' {
        printf("FORSTART%d:\n", $2);
    } stmt {
        pop_up_symbol(cur_scope--);
        breakindex--;
        printf("\tjal zero, FORLOOP%d\n", $2);
        printf("FOREND%d:\n", $2);
    }
    |RETURN expr ';'{
        get_one_expr_to_temp($2, 0);
        printf("\tadd a0, t0, zero\n");
        free_symbol(table[functor_index].scope-1);
        printf("\tjal zero, END%s\n", functor_name);
    }
    |RETURN ';'{
        free_symbol(table[functor_index].scope-1);
        printf("\tjal zero, END%s\n", functor_name);
    }
    |BREAK ';'{
        free_symbol(cur_scope);
        switch(break_table[breakindex-1].type){
            case FORBREAK:
                printf("\tjal zero, FOREND%d\n", break_table[breakindex-1].num);
                break;
            case WHILEBREAK:
                printf("\tjal zero, WHILEEND%d\n", break_table[breakindex-1].num);
                break;
            case DOBREAK:
                printf("\tjal zero, DOEND%d\n", break_table[breakindex-1].num);
                break;
        }
    }
    |CONTINUE ';'{}
    |compound_stmt{}
;

init_for: '(' {
    cur_scope++;
    $$ = fornum++;
    break_table[breakindex].type = FORBREAK;
    break_table[breakindex].num = $$;
    breakindex++;
}

init_while: '(' {
    printf("WHILESTART%d:\n", whilenum);
    $$ = whilenum++;
    cur_scope++;
    break_table[breakindex].type = WHILEBREAK;
    break_table[breakindex].num = $$;
    breakindex++;
}

init_do: DO {
    printf("DOSTART%d:\n", donum);
    $$ = donum++;
    cur_scope++;
    break_table[breakindex].type = DOBREAK;
    break_table[breakindex].num = $$;
    breakindex++;
}

ifelse_stmt:
    IF '(' con_expr if_branch compound_stmt %prec IFX{
        pop_up_symbol(cur_scope--);
        printf("MYELSE%d:\n", $4);
    }
    |IF '(' con_expr if_branch compound_stmt {
        pop_up_symbol(cur_scope);
        printf("\tjal zero, MYEXIT%d\n", $4);
        printf("MYELSE%d:\n", $4);
    }ELSE compound_stmt{
        pop_up_symbol(cur_scope--);
        printf("MYEXIT%d:\n", $4);
    }
;

con_expr: expr {
    if($1 < 0) printf("\tld t0, %d(s0)\n", $1); 
    else if($1 < 1000){
        printf("\tld t0, 0(sp)\n");
        printf("\taddi sp, sp, 8\n");
        for(int i = 0; i < $1; i++) printf("\tld t0, 0(t0)\n");
    }
}

if_branch: ')' {
    printf("\tbeqz t0, MYELSE%d\n", ifnum);
    $$ = ifnum++;
    cur_scope++;
}

switch_clauses:
    switch_clauses switch_clause {}
    |switch_clause {}
;

switch_clause:
    CASE expr ':' switch_stmt {}
    |CASE expr ':'{}
    |DEFAULT ':' switch_stmt {}
    |DEFAULT ':' {}
;

switch_stmt:
    switch_stmt stmt {}
    |stmt {}
;

for_expr:
    expr {if($1 >= 0) printf("\taddi sp, sp, 8\n"); else printf("\n");}
    |{printf("\t//empty for_expr\n");}
;

compound_stmt:
    '{' compound_stmt_children '}' {}
    |'{' '}'{}
;

compound_stmt_children:
    compound_stmt_children stmt {printf("\n");}
    |compound_stmt_children scalar_decl {printf("\n");}
    |compound_stmt_children array_decl {printf("\n");}
    |stmt {printf("\n");}
    |scalar_decl {printf("\n");}
    |array_decl {printf("\n");}

;
%%
    int main()
    {
        init_symbol_table();
        yyparse();
        return 0;
    }
        int yyerror(char *msg)
    {
        fprintf(stderr, "Error encountered: %s \n", msg);
        return 0 ;
    }
	int yylex();

